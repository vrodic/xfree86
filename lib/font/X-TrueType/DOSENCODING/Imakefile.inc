XCOMM $XFree86$

#define ModuleName DOSENCODING
MODULENAME = ModuleName
MODULESRCDIR = $(XTTSOURCEDIR)/$(MODULENAME)

SRCS =	main.c DOSENCODINGtoUCS2.c
OBJS =	main.o DOSENCODINGtoUCS2.o

LinkSourceFile(main.c,$(MODULESRCDIR))
LinkSourceFile(DOSENCODINGtoUCS2.c,$(MODULESRCDIR))
