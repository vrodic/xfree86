The FreeType compilation installation guide


Introduction

I. Quick compilation

  1. Unix
  2. OS/2
  3. MS-DOS
  4. Amiga

II. Package layout

  1. The library
  2. The test programs

III. Compilation steps

  1. Compiling the library
  2. Compiling the graphics subsystem & driver
  3. Compiling the test programs

IV. Test programs usage and additional notes

  1. ftzoom
  2. ftlint
  3. ftview
  4. fttimer
  5. ftdump
  6. ftstring
  7. ftstrpnm
  8. fterror


--------------------------------------------------------------------

Introduction:

  This file  describes the steps  that are necessary to  compile the
  files found  in this package.  Section I  contains instructions to
  compile   the   package   'out-of-the-box'   on   a   variety   of
  platforms/compilers.  Section II gives  the detailed layout of the
  package's  files,  a  necessary  understanding  for  the  detailed
  compilation steps given in section III.

  Note  that more  detailed  system-specific versions  of this  file
  exist in the "freetype/howto"  directory.  You may consult them if
  you encounter problems on your platform.

  This package, known as  the  FreeType 1.2 Public Release, contains
  several things:

  - the 1.2 release of the  FreeType library, contained in the 'lib'
    directory.   The library  is  written in  portable  ANSI C.   It
    should compile fine with any type of ANSI C compiler.

    Note  that 16  bit compilers  are not  fully supported  yet (the
    library compiles, though), as  some problems may arise with very
    large point sizes, or large  font files.  However, code has been
    severely checked for errors lately.

  - a  set of  test programs,  found in  the directory  'test' which
    provide:

    - A  small  graphics  subsystem  used  to  display  the  bitmaps
      produced by the  library, on a great variety  of systems.  The
      subsystem is made of several files:

      - a  set  of common  files  (gmain.h,  gmain.c, gdriver.h, and
        gevents.h)

      - some system-specific drivers used to implement the interface
        found in 'gdriver.h'  and 'gevents.h' (gfs_dos.c, gfs_os2.c,
        gwin_x11.c,  etc).  The  X11  driver is  located in  "test",
        while   others    are   located   in   architecture-specific
        directories (example: "test/arch/os2/gpm_os2.c").

      - "common.h/c" provide  the 'basename' and  'getopt' functions
        for systems which do not provide it already.

    - Several test  programs which use the library  and the graphics
      subsystem:

      - ftzoom:   A simple  glyph viewer.  Supports  composite glyph
                  loading, font smoothing.

      - ftlint:   A  simple  glyph  instruction verifier.   Used  to
                  execute  all glyphs instructions  found in  a font
                  file at a given ppem size.  Useful to find bugs in
                  the library and/or font files.

      - ftview:   A simple font viewer.  Supports font smoothing and
                  hinting!

      - fttimer:  Performance timer used  to benchmark the scan-line
                  converter.   Supports display  and font-smoothing.
                  This is _not_ an  indication of FreeType's overall
                  performance!

      - ftdump:   A very simple font information dumper.

      - ftstring: A  simple program  to  display a  given string  or
                  message in a given font, at a given point size.

      - ftstrpnm: Converts a given text string rendered with a given
                  font at a given size  into PBM or PGM format which
                  can  be further  converted  into popular  graphics
                  formats  like  GIF or  PNG  with  the netpbm  tool
                  suite.

      - fterror:  Tests   the   GNU   gettext()  functionality   for
                  internationalized message.

  - an OS/2 font  driver for TrueType fonts to  replace the original
    TRUETYPE.DLL (which is  of poor quality).  It is  located in the
    contrib/ftos2 subdirectory.

  - a  conversion  tool, called  ttf2bdf,  which  uses the  FreeType
    library to produce bitmaps in  BDF format (used with X Windows),
    located  in   the  directory  "contrib/ttf2bdf".    It  is  only
    supported on Unix for now.

  - another conversion tool, called ttf2pk, used to convert TrueType
    fonts  to  TeX's  PK   font  format,  in  "contrib/ttf2pk".   It
    currently needs the kpathsea library  (which is part of web2c, a
    popular  TeX  implementation) for  TeX  file  searching, but  it
    should be easy to replace the searching routines.


--------------------------------------------------------------------

I. Quick compilation:

  NOTE: With  the exception of Unix,  all system-dependent makefiles
        and  configuration  files  are  located in  the  directories
        lib/arch/<platform>/ and test/arch/<platform>/.

  1. Unix:

    We provide  a 'configure' script that  will automatically create
    configuration  and Makefile files  in all  relevant directories.
    Creation  of  shared  libraries  is  supported;  say  'configure
    --help' for configuration options.

    Graphics display is performed through X11 only, which means that
    the performance measured by 'timer' with display 'on' should not
    be considered as 'informative'.

    We have tested FreeType and  its tools with the gcc compiler; if
    you use a platform specific  compiler or make program which need
    special compilation  switches please report this  to our mailing
    list so we can incorporate this into the next release.

    Go to your 'freetype' directory, type './configure', 'make', and
    'make install'.  Other available  targets are `depend', `clean',
    `distclean', and `uninstall'.

    Please jump to section IV for usage descriptions.

    Please read  sections  II and  III to  learn how to  compile the
    package for your  own system in case the  configure script fails
    for your system.

    Note: If  FreeType  is  compiled   with  the  GNU  gettext  i18n
          extension (via ftxerr18.c), you have to use '-lintl -lttf'
          for linking with the FreeType library.   Type `./configure
          --help' to  see the available configuration  options;  you
          can set the installation path  for message catalogues with
          the `--with-locale-dir' option.


  2. OS/2:

    Makefile.dm:  This is for gcc/emx + dmake.

                  It builds  both full  screen and PM  windowed test
                  versions.

                  To use it, go to the 'test' directory and type:

                     make -r -f arch/os2/Makefile.dm


    Makefile.emx: This is for GNU make and gcc/emx.

                  It builds a version  of the test programs that use
                  a PM  display window (xxx.exe) and  a version that
                  uses the full-screen display driver (xxxfs.exe).

                  To use it, go to the 'test' directory and  type:

                     make -f arch/os2/Makefile.emx


    Makefile.icc: This  file  must  be used with Visual Age C++.  It
                  now  supports full-screen  and  PM display.   Note
                  that  the library  is compiled  with optimizations
                  OFF, due to a compiler bug which makes icc compile
                  the file 'ttraster.c'  erroneously.  This bug will
                  be reported to IBM.

                  To use it, go to the 'test' directory and type:

                     nmake -f arch\os2\Makefile.icc


    Makefile.wat: This file must be used with Watcom.   It  supports
                  PM  and fullscreen output alike.

                  To use it, go to the 'test' directory and type:

                     wmake -f arch\os2\Makefile.wat


    In addition, the following make targets can be invoked:

      * clean:     Erases all object and library files, only keeping
                   the executables in place.

      * distclean: Same as 'clean', but also erase executables.


    For other compilers, please read sections II and III to know how
    to compile  the package on  your system.  You're welcome  to add
    new makefiles and  send them to us, as long  as they respect our
    current location conventions.


  3. MS-DOS and clones:

    Makefile.dm:  This is for gcc/emx + dmake.

                  To use it, go to the 'test' directory and type:

                     make -r -f arch/msdos/Makefile.dm


    Makefile.gcc: This is for GNU make and gcc/emx.

                  To use it, go to the 'test' directory and type:

                     make -f arch/msdos/Makefile.gcc


    In addition, the following make targets can be invoked:

      * clean:     Erases all object and library files, only keeping
                   the executables in place.

      * distclean: Same as 'clean', but also erase executables.


    The two previous makefiles should work with djgpp too.


    Note that you may have to set some emx options (usually -acm) in
    the  EMXOPT  environment  variable  to  run  the  test  programs
    compiled with emx.

    There is only a full-screen display driver for MS-DOS.

    For other compilers, please read sections II and III to know how
    to compile  the package on  your system.  You're welcome  to add
    new makefiles and  send them to us, as long  as they respect our
    current location conventions.

    `char' is always `signed char' in the sources!


  4. Amiga:

    You will need gcc and GNU make for Amiga to compile the package.
    Go to the 'test' directory, then type:

      make -f arch/amigaos/Makefile.gcc

    The arch/amigaos  contains two display drivers  (full screen and
    windowed).  The default driver is the windowed one, but this can
    be changed easily in the makefile.

    For other compilers, please read sections II and III to know how
    to compile the package on your system. You're welcome to add new
    makefiles  and send  them to  us, as  long as  they  respect our
    current location conventions.


--------------------------------------------------------------------

II. Package Layout:

  Here is  a more  thorough description of  the package's  files and
  layout.

  # The 'test' directory:

    Contains the graphics subsystem's common files.

      gmain.h
      gmain.c
      gdriver.h
      gevents.h

    where 'gdriver.h'  is the interface that must  be implemented by
    all display drivers used by the subsystem.

    Note that the  graphics subsystem is _not_ part  of the library,
    it's just a  way we found to display  the engine's production on
    several systems.

    The platform-specific driver  (which means the implementation of
    "gdriver.h") is found  in the architecture-dependent directories
    of the form:

      test/arch/<platform>

    where <platform> can be either "msdos", "amiga", "os2", etc.

    We  provide  several  display  driver  implementations  on  some
    platforms:

      test/arch/os2/gfs_os2.c       - full-screen OS/2 driver
      test/arch/os2/gpm_os2.c       - PM OS/2 driver

      test/arch/amigaos/gfsamiga.c  - full-screen Amiga driver
      test/arch/amigaos/gw_amiga.c  - windowed Aniga driver

      test/arch/msdos/gfs_dos.c     - full-screen DOS driver

      test/arch/unix/gwin_x11.c     - windowed X11 driver

    You'll need to compile one of these drivers for your system, and
    link  it  to  the  test  programs  together  with  the  compiled
    'gmain.c'.

    It also contains several test files:

       common.c   -- common  subroutines uses  by all test programs.
                     This file  defines and implement  the functions
                     "basename" and "getopt" if they're not provided
                     by your environment.

       display.c  -- a  small  lib  used  by several  test  programs
                     wrapping around  the platform specific graphics
                     code.

       ftview.c   -- font viewer
       fttimer.c  -- performance tester
       ftzoom.c   -- simple glyph viewer
       ftstring.c -- simple string text generation program
       ftstrpnm.c -- converts a rendered text string into the PBM or
                     PGM graphics file format

       ftlint.c   -- font instruction checker -- console mode only
       ftdump.c   -- simple font dumper -- console mode only

       fdebug.c   -- short debugger;  you need to  compile ttdebug.c
                     and  ttins.c in  the c/lib  directory  with the
                     DEBUG  constant defined to  use it.   Text mode
                     only.  Makes sense for development only.  Avoid
                     it completely if  you  don't need  to  hack the
                     engine.

       fterror.c  -- testing gettext() functionality under UNIX.

  # The 'lib' directory:

    Contains the engine's source code.

    The engine  is made of  several components, each one  having its
    name  beginning with  a 'tt'  prefix like  'ttmemory', 'ttfile',
    'ttraster', etc.

    All components rely on the 'freetype.h' interface file (which is
    the  engine's  interface  to   be  used  by  all  clients),  and
    'ttconfig.h' (which is used to generate configuration macros).

    Note  that   'ttconfig.h'  relies  heavily  on   a  file  called
    'ft_conf.h'  that  contains information  related  to the  target
    platform.  The  ft_conf.h file  is generated by  the 'configure'
    script  under Unix.   For other  systems, it  is located  in the
    lib/arch/<platform>/ directory:

       lib/arch/os2/ft_conf.h
       lib/arch/amigaos/ft_conf.h
       lib/arch/msdos/ft_conf.h

    It should be pretty obvious  to create a new ft_conf.h file from
    a current one: just copy  it and define/undefine the macros that
    are relevant to your platform.

  # The 'lib/extend' directory:

    This directory  contains the current core engine extensions.  To
    know  more  about  extensions,  read  the user  guide  found  in
    "docs/user.txt".


--------------------------------------------------------------------

III. Detailed Compilation Steps:

  You must  perform the following  steps to compile this  package by
  hand:

  1. Compile the library:

    with gcc, go to the 'lib' directory, then type

      gcc -c <options> tt*.c -I..
      gcc -c <options> extend/ftx*.c -I.. -Iextend

    for Unix systems or

      gcc -c <options> tt*.c -Iarch/<platform>
      gcc -c <options> extend/ftx.c

    for other platforms.

    <options>  stands for '-Wall  -ansi -pedantic  -O2' for  the gcc
    compiler.

     The flags mean:

       -c        : compile only

       -Wall     : display all warnings during compilation

       -ansi     : check for ANSI conventions

       -pedantic : be _very_ pedantic when checking ANSI convention

       -O        : optimization level (this is the letter capital O)

       -I        : add  an include  dir

                   This  line  is  needed  to  give  access  to  the
                   directory containing 'ft_conf.h' and the 'extend'
                   directory.


  2. Compile the graphics subsystem:

    a. compile the subsystem in the 'test' dir:

        gcc -c <options> gmain.c

    b. compile the display driver in the 'test' dir:

        gcc -c <options> gwin_x11.c -o gdriver.o

      for Unixish systems (see note below) or

        gcc -c <options> arch/system/yourdriver.c -o gdriver.o

      for other platforms.  Of course,  don't forget to add the path
      to your system's display libraries.

      <options> usually stands for '-Wall -ansi -O2'; '-o' specifies
      the output name of the ojlect file (here 'gdriver.o').


  3. Compile the test programs in the 'test' directory:

    Usually with something like:

        gcc <options> testprogram.c \
            ../lib/*.o gmain.o gdriver.o \
            -I<path_to_ft_conf.h> -I../lib

    with  additional libraries ('\'  indicates a  line continuation;
    <options> as in the previous section).

    Have a  look at the supplied  makefiles to get a  better idea of
    the required flags/commands.

    You should now be able to run the test programs.


----------------------------------------------------------------------------

IV. Test programs usage:

  NOTE: All  test programs having  a graphic interface use  the same
        key convention:

    x :   fine counter_clockwise rotation (ftzoom only)
    c :   fine clockwise rotation         (ftzoom only)

    v :   fast counter_clockwise rotation (ftzoom only)
    b :   fast clockwise rotation         (ftzoom only)

    h :   toggle hinting on/off

    + :   fast scale up
    - :   fast scale down
    u :   fine scale up
    j :   fine scale down

    l :   go to next glyph
    k :   go to previous glyph

    o :   go to tenth next glyph
    i :   go to tenth previous glyph

    0 :   go to hundredth next glyph (useful for CJK fonts)
    9 :   go to hundredth previous glyph

    ) :   go to thousandth next glyph
    ( :   go to thousandth previous glyph

    } :   go to tenthousandth next glyph
    { :   go to tenthousandth previous glyph
    

    q :
  ESC :   exit

  These  keys  were  chosen   because  they  are  available  on  all
  platforms.  Note also that each program uses only a subset of this
  key map.


  1. FTZOOM:

    'ftzoom'  is  a very  simple  glyph  viewer  that supports  font
    smoothing.  Its usage is:

        ftzoom [-g] [-p platformID -e encodingID] 
               [-r resolution] [-z magnification] [-n] fontfilename

    With -g you can  select gray-scaling;  with -n you can  suppress
    usage of the `post' table in the TrueType font.

    For example:

        ftzoom arial.ttf             to show the glyphs found
                                     in the Arial font
        ftzoom -g times.ttf          to show smoothed version of
                                     Times's glyphs.
        ftzoom -p 3 -e 1 uwjmg3.ttf  use cmap for platform ID 3,
                                     encoding ID 1 for this Japanese
                                     font.

  2. FTLINT:

    FTLint is  used to  execute all glyphs  instructions found  in a
    font file at a given char size size.  Its usage is:

        ftlint pointsize fontfilename [fontfilename] ...

    For example:

        ftlint 12 arial.ttf
        ftlint 15 times.ttf

    It  reports error  codes and  faulty glyph  numbers.  This  is a
    console tool that doesn't need the graphics subsystem.

    Since  it is  a  production  tool, don't  get  surprised to  see
    numerical error codes (the  list is in the file 'lib/freetype.h'
    anyway).

    NOTE: Trying to hint at sizes smaller than 7 is irrelevant.

  3. FTVIEW:

    A  font viewer that  supports hinting  and font  smoothing.  Its
    usage:

        ftview [-g] pointsize fontfilename

    like in:

        ftview 12 arial.ttf      show the hinted Arial at size 12pt.

        ftview -g 15 timesi.ttf  show the hinted and font-smoothed
                                 Times at size 15pt.

    Note that it is possible to change the point size during display
    with the  keys 'u', 'j', '+',  and '-'.  It is  also possible to
    browse the  whole glyph  set with the  'k', 'l', 'i',  'o', '0',
    '9', '(', ')', '{', and '}' keys (see key map above).

    The OS/2  PM driver comes with an  additional magnifying window.
    The magnified  zone is  set with the  mouse, while the  scale is
    changed with the help of 'PageUp' and 'PageDown'.

    Note 1: the  engine uses the font's CVT  program to determine at
            which size to enable or disable glyph hinting.  Don't be
            surprised to see unhinted glyphs at sizes < 7pt.

    Note 2: vertical drop-out  control has  been  added to  the gray
            scaling renderer.  As a consequence, the library doesn't
            produce  unpleasant results  at small  ppems  with badly
            hinted glyphs.

  4. FTTIMER:

    This program is used to benchmark FreeType's scan-converter (the
    component in charge of translating a vectorial shape description
    into a bitmap).  It does so in preloading all glyphs from a font
    file,  then rendering  them as  fast  as possible  in a  640x450
    buffer.  The glyphs are rendered at size 400pt / 96dpi, which is
    _quite_ big.

    Its usage is:

        timer [-g] [-v] fontfilename

    where

        -g : asks for  gray-level  rendering  at size  200pt instead
             (a.k.a. font-smoothing)

        -v : asks for  the display of  the produced  bitmap.  Beware
             that  display  slows things  down  (display  on X11  is
             _terrible_,  especially with  gray-levels, but  this is
             not a problem for us :-).

    Note that the returned numbers are not a becnhmark of FreeType's
    overall  performance!   Only of  the  scan-line renderer  (which
    seems quite fast, apparently :-).

    When  comparing measured  performances  across machines,  please
    only  consider the  undisplayed ones.   We're interested  in all
    kinds of results (please provide the following information:

        font file
        number of rendered glyphs
        render time
        total time
        glyphs / s
        processor type
        CPU clock

        and which  compiler used (with  optimization involved)! This
        can make a great difference!

        etc.)

  5. FTDUMP:

    This  program is  a very  simple  font dumper.   In its  current
    incarnation, it will only  output a font's name table, character
    encoding maps IDs and total memory consumption.

    The "total memory  used" reported is the amount  that is used by
    the engine to load one face, with one instance (a point size).

    Each additional  instance takes only a fraction  of that amount,
    and is labeled  by "instance object".  As you  can see, FreeType
    isn't really greedy.

       Usage:   ftdump fontpathname[.ttf|.ttc]


  6. FTSTRING:

    This  program  demonstrates  string  text generation.   It  only
    displays a given  message on the screen, and  lets you resize it
    with the classic key bindings 'u', 'j', '+', and '-'.

    Usage:

        ftstring  ptsize  fontname[.ttf|.ttc]  [message_string]

    If  the  message is  omitted,  it  will  revert to  the  classic
    typographic test sentence:

        The quick brown fox jumps over the lazy dog

    which is made of all letters of the English alphabet.

    Ftstring  only works  with  font files  that  contain a  Unicode
    character  mapping  table   (either  Windows  Unicode  or  Apple
    Unicode).   For the  moment,  the message  can  however only  be
    written in ASCII, as accents aren't supported yet.


  7. FTSTRPNM:

    Usage:

        ftstrpnm [options] filename [string]

    Options:

      -g      gray-level rendering (default: off)
      -h      hinting off (default: on)
      -r X    resolution X dpi (default: 96)
      -p X    pointsize X pt (default: 12)
      -b X    border X pixels wide (default: 0)

    This  program is  quite  similar to  ftstring  but converts  the
    rendered image of  the specified string into a  bitmap in PBM or
    PGM  format written  to stdout;  PBM (Portable  BitMap)  and PGM
    (Portable GrayMap)  formats can be further  converted to popular
    graphics  formats like  GIF or  PNG with  the netpbm  tool suite
    available via Internet.
    

  8. FTERROR:

    This program tests the gettext() functionality on UNIX platforms
    (usually provided  in the GNU gettext package).   It will return
    language specific  error and warning messages  depending on your
    locale.   Currently  French   and  German  translations  of  the
    FreeType messages  are included --  we invite you  to contribute
    more translations.

    Typically, you have to set the LANG environment variable to your
    locale to get localized messages.  Example:

      LANG=de fterror

    Note  that  providing message  strings  for  FreeType's error  and
    warning  messages is  an extension  and not  part of  the FreeType
    library  itself.   Please   refer  to  docs/i18n.txt  for  further
    details.

--- end of HOWTO ---
