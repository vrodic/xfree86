Welcome to the documentation directory.

Most of the  files here are obsolete or  will undergo great changes!
However, the following titles are up-to-date and useful:


  ..................................................................

  apiref.txt    FreeType high-level  API reference.  All  the types,
                functions and error codes of FreeType

  ..................................................................

  user.txt      FreeType's  user  guide.    A  guide  to  FreeType's
                concepts, a  step by step example, and  a note about
                future works and projects.

  ..................................................................

  FAQ           The new up-to-date FreeType FAQ. Look here first if 
                you have problems with the library.

  ..................................................................

  convntns.txt  The  FreeType developer's conventions  guide.  Lists
                the  design  and  coding  conventions  used  in  the
                engine's  source  code.  This file is incomplete but
                contains information  that  should  be  read  by any
                person willing to hack the FreeType code.

  ..................................................................

  porting.txt   The  FreeType  porting   guide.   All  you  need  to
                configure  and adapt  very  easily FreeType's source
                code to tailor it to your own platform.

                Learn how to provide  your own memory, i/o and mutex
                components, best fitted to your system.  See how you
                can   build   a   singly-threaded,  thread-safe   or
                re-entrant  version  of  the  engine from  the  same
                source code!

  ..................................................................

  raster.doc    The  scan-line converter's internals  explained. The
                document is  still incomplete but  presents many key
                concepts.


Soon to come:   These  documents will probably  come later.

 ..................................................................

  develop.txt   The FreeType developer's guide. All you need to know
                in order to be able  to understand and even hack the
                engine's core source code.

  ..................................................................

  extend.txt    The FreeType extension writer's how-to. All you need
                to be able to provide new extensions to the engine.

                This  information  is  currently  available  in  the
                "conventions" file

  ..................................................................

  undoc.txt     Undocumented TrueType!

                Yes,    ladies   and    gentlemen,    the   TrueType
                specification  is  fuzzy  and misleading  enough  to
                disband  some  serious  commercial  developers,  but
                that's wasn't  enough for  us.  Even though  it took
                some time (aaargh...), we finally discovered through
                hard work the real meanings of the specs, fixed some
                errors   and  even  found   surprising  undocumented
                behaviour or features!

