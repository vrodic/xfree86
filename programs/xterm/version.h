/* $XFree86: xc/programs/xterm/version.h,v 3.91 2003/05/21 22:59:14 dickey Exp $ */

/*
 * These definitions are used to build the string that's printed in response to
 * "xterm -version", or embedded in "xterm -help".  It is the version of
 * XFree86 to which this version of xterm has been built.  The number in
 * parentheses is my patch number (T.Dickey).
 */
#define XTERM_PATCH   180
#define XFREE86_VERSION "XFree86 4.3.99.15"
